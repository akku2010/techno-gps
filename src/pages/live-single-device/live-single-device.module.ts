import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LiveSingleDevice } from './live-single-device';
import { IonBottomDrawerModule } from 'ion-bottom-drawer';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    LiveSingleDevice,
  ],
  imports: [
    IonicPageModule.forChild(LiveSingleDevice),
    IonBottomDrawerModule,
    TranslateModule.forChild()
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class LiveSingleDeviceModule { }
